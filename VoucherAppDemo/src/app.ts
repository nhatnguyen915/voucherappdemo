import { DecorationMethod, ResponseToolkit, Server } from "@hapi/hapi"
import hapiAuthJwt2 from "hapi-auth-jwt2";
import { validate } from "./common/jwtService";
import { LoginInterface } from "./models/interface/requestInterface";
import { router } from "./routers/router";
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');

let accounts = {
    user: {
        id: 123,
        userName: 'NhatNguyen',
        password: 'Nhatnguyen915',
    }
};
export const init = async () => {
    const server = new Server({
        port: 3000,
        host: 'localhost'
    });
    const swaggerOptions = {
        info: {
            title: 'Test API Documentation 123',
            // version: Pack.version,
        },
        auth: 'jwt'
    };
    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: {
                info: {
                    title: 'Doc',
                },
                schemes: ["http", "https"],
                securityDefinitions: {
                    'jwt': {
                        'type': 'apiKey',
                        'name': 'Authorization',
                        'in': 'header',
                        'x-keyPrefix': 'Bearer '
                    }
                },
                security: [{ jwt: [] }],
                //auth: hapiAuthJwt2

            }
        }
    ]);
    await server.register(require('hapi-auth-jwt2'));
    server.auth.strategy('jwt', 'jwt',
        {
            key: 'BbZJjyoXAdr8BUZuiKKARWimKfrSmQ6fv8kZ7OFfc', // Never Share your secret key
            validate
        });

    server.auth.default('jwt');

    router(server);
    await server.start();
    console.log('server running on %s', server.info.uri);
}