import { Request, ResponseObject, ResponseToolkit } from "@hapi/hapi";
import Event from "../models/Event";
import Voucher from "../models/Voucher";
import { startSession } from "mongoose"
const { success, error, validation } = require("../dto/responseApi");
import { EditableInterface, LoginInterface } from "../models/interface/requestInterface";
import { editableReleaseJob, editableMaintainJob } from '../agenda/agenda.handler'
import { addEmailToQueue } from "../bull/handler/email.handler";
import UpdateTracking from "../models/UpdateTracking";
export const createEvent = async (request: Request, h: ResponseToolkit) => {
    try {
        //  const event= Event.create([request.payload])
        // const eventUpdate =await  Event.findOne({event_name:'EV5555'})
        const time = new Date().getTime();
        console.log(`Time request :::: ${time} `);
        return h.response(success("OK", { data: "Some random data" })).code(200);
    } catch (e) {
        return h.response(error("OK", { data: e })).code(500);
    }
};
export const generateVoucher = async (request: Request, h: ResponseToolkit) => {
    const i = Math.random()
    const session = await startSession();
    try {
        session.startTransaction();
        //  let event = await Event.findById(request.params.event_id).session(session);
        let eventUpdate = await Event.findOneAndUpdate(
            { _id: request.params.event_id },
            { $inc: { issued: 1 } },
            { session, new: true }
        );

        if (eventUpdate.issued > eventUpdate.max_quantity) {
            console.log("Sould out !!!")
            throw new Error();
        }
        else {
            const voucher = await Voucher.create([{ event_id: request.params.event_id, code: i, value: i }], { session });
            console.log(`Vouchers had issued: ` + eventUpdate.issued);
            addEmailToQueue();
            await session.commitTransaction();
            await session.endSession();
            return h.response(success("OK", { data: voucher })).code(200);
        }
    } catch (error) {
        await session.abortTransaction()
        session.endSession();
        return h.response().code(456);
    }
}
export const editableMe = async (request: EditableInterface, h: ResponseToolkit) => {
    const session = await startSession();
    try {
        let errorCode: number;
        session.startTransaction();
        let event = await Event.findById(request.payload.eventId).session(session);
        console.log(request.auth.credentials.userId);
        if (event.isEditable) {
            event.user_editing == 1 ? errorCode = 200 : errorCode = 409;
            await session.commitTransaction();
            await session.endSession();
            return h.response().code(errorCode)
        }
        else {
            const eventUpdate = await Event.findOneAndUpdate(
                { _id: request.payload.eventId },
                {
                    isEditable: 1,
                    user_editing: 1,
                    editingCount: event.editingCount + 1
                },
                { session, new: true }
            );
            if (eventUpdate.editingCount > 1) {
                throw new Error();
            }
            await session.commitTransaction();
            session.endSession();
            await editableReleaseJob(request.payload.eventId);
            return h.response().code(200);
        }
    } catch (error) {
        await session.abortTransaction()
        session.endSession();
        return h.response().code(409);
    }
}
export const editableMeV2 = async (request: EditableInterface, h: ResponseToolkit) => {
    const session = await startSession();
    try {
        const userId = request.auth.credentials.userId;
        let errorCode: number;
        session.startTransaction();
        let updateTracking = await UpdateTracking.findOne({ 'eventId': request.payload.eventId }).session(session);
        if (!updateTracking) {
            await UpdateTracking.create([{ userId: userId, eventId: request.payload.eventId, updatedTime: new Date() }], { session });
        }
        else {
            if (updateTracking.userId = request.payload.userId)
                return h.response().code(200);
            else
                return h.response().code(409);
        }


        const count = await UpdateTracking.countDocuments({ 'eventId': request.payload.eventId }).session(session);
        if (count > 1) {
            throw new Error();
        }
        await session.commitTransaction();
        session.endSession();
        await editableReleaseJob(request.payload.eventId);
        return h.response().code(200);
    } catch (error) {
        await session.abortTransaction()
        session.endSession();
        return h.response().code(409);
    }
}
export const editableRelease = async (request: EditableInterface, h: ResponseToolkit) => {
    try {
        const userId = request.auth.credentials.userId;
        const event = await UpdateTracking.deleteOne({ 'eventId': request.payload.eventId, 'userId': userId });
        return h.response().code(200);
    } catch (error) {
        return h.response().code(409);
    }
}
export const editableMaintain = async (request: EditableInterface, h: ResponseToolkit) => {
    try {
        const userId = request.auth.credentials.userId;
        const event = await UpdateTracking.deleteOne({ 'eventId': request.payload.eventId, 'userId': userId });
        const updateTracking = await UpdateTracking.create([{ userId: request.payload.userId, eventId: request.payload.eventId, updatedTime: new Date() }]);
    } catch (error) {
        return h.response().code(409);
    }
}


