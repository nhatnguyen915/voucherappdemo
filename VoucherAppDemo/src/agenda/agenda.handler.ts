import Agenda, { Job, JobAttributesData } from "Agenda"
import { editableRelease } from "../controllers/controller";
import Event from "../models/Event";
const connectionString = 'mongodb+srv://nhatnguyen915:P%40ssword1234@cluster0.ubnbi.mongodb.net/HapiDemo?retryWrites=true&w=majority';

export const agenda = new Agenda({
    db: { address: connectionString },
    processEvery: '30 seconds'
});

agenda.define('editableRelease', async ({ attrs }: Job) => {
    console.log("eventId" + attrs.data);
    const eventId = attrs.data;
    console.log(eventId)
    try {
        await Event.findOneAndUpdate(
            { _id: eventId },
            {
                isEditable: 0,
                user_editing: 0,
                editingCount: 0
            }
        );
        return;
    } catch (error) {
    }
});
export const editableReleaseJob = async function (eventId: string) {
    await agenda.start();
    await agenda.schedule('in 5 minutes', eventId, eventId);
};

export const editableMaintainJob = async (eventId: string) => {
    await agenda.cancel({ 'name': eventId });
    await editableReleaseJob(eventId);
}