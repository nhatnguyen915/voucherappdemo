'use strict';
import Joi from "joi"
export const voucherSchema = Joi.object({
    event_id: Joi.string,
    code: Joi.string,
    value: Joi.number
});
export const voucherResponseSchema = Joi.object({
    message: Joi.string().default("OK"),
    error: Joi.boolean().default(false),
    code: Joi.number().default(200),
    results: Joi.object({
        event_id: Joi.string().default("EV01"),
        code: Joi.string().default('22222FFF'),
        value: Joi.number().default(1000)
    }),

});

