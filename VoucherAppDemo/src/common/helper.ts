import Voucher from"../models/Voucher";
 export let edit_event_m=new Map();
export let timeout_m= new Map();

export function createVoucher (eventId:number) :void{
    let code = makeCode(6);
    let value=Math.floor(Math.random()*(999-100+1)+100);
    Voucher.create({event_id:eventId,code:code,value:value});
}
function makeCode(length:number) {
    let result           = '';
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}
export function remove_event_m(event_id:string):void  {
    edit_event_m.delete(event_id)
}

