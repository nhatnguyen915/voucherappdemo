import { ResponseToolkit, server, Server } from '@hapi/hapi'
import { createEvent, editableMaintain, editableMe, editableMeV2, editableRelease, generateVoucher } from '../controllers/controller'
import Joi, { isSchema } from 'joi';
import { addEmailToQueue } from '../bull/handler/email.handler'
import { login, register } from '../controllers/user.controller';
import { voucherResponseSchema } from '../dto/ResponseData';
const { success, error, validation } = require("../dto/responseApi");
export const router = (server: Server) => {
    server.route({
        method: 'GET',
        path: "/email",
        options: {
            tags: ['api', 'person']
        },
        handler: addEmailToQueue

    }),
        server.route({
            method: 'GET',
            path: "/voucher/{event_id}",
            options: {
                auth: false,
                tags: ['api', 'voucher'],
                validate: {
                    params: Joi.object({
                        event_id: Joi.string().required(),
                    }).options({ stripUnknown: true }),
                },
                response: {
                    schema: voucherResponseSchema
                }

            },
            handler: generateVoucher,

        }),
        server.route({
            method: "POST",
            path: "/events/editable/me",
            options: {
                auth: 'jwt',
                tags: ['api', 'events-editable'],
                validate: {
                    payload: Joi.object({
                        eventId: Joi.string().default("EV22222").required().description("eventId of event"),
                        userId: Joi.number().min(0).default(1).required().description("userId ")
                    }).options({ stripUnknown: true })
                },
                handler: editableMe
            },

        }),
        server.route({
            method: "POST",
            path: "/events/editable/release",
            options: {
                auth: false,
                tags: ['api', 'events-editable-release'],
                validate: {
                    payload: Joi.object({
                        eventId: Joi.string().default("EV22222").required().description("eventId of event"),
                        userId: Joi.number().min(0).default(1).required().description("userId ")
                    })
                },
                handler: editableRelease,
            },

        }),
        server.route({
            method: "POST",
            path: "/events/editable/maintain",
            options: {
                tags: ['api', 'events-maintain'],
                validate: {
                    payload: Joi.object({
                        eventId: Joi.string().default("EV22222").required().description("eventId of event"),
                        userId: Joi.number().min(0).default(1).required().description("userId ")
                    }).options({ stripUnknown: true })
                },
                handler: editableMaintain
            },

        })
    server.route([{
        method: "POST",
        path: "/login",
        options: {
            auth: false,
            tags: ['api', 'events-maintain'],
            validate: {
                payload: Joi.object({
                    userName: Joi.string().required(),
                    password: Joi.string().required(),
                }).options({ stripUnknown: true })
            },
        },
        handler: login
    }]);
    server.route([{
        method: "POST",
        path: "/register",
        options: {
            auth: false,
            tags: ['api', 'register'],
            validate: {
                payload: Joi.object({
                    userName: Joi.string().required(),
                    password: Joi.string().required(),
                }).options({ stripUnknown: true })
            },
        },
        handler: register
    }]);
    server.route([{
        method: "GET",
        path: "/test-jwt",
        options: {
            auth: 'jwt',
            tags: ['api', 'events-maintain'],
        },
        handler: createEvent
    }]);
}

