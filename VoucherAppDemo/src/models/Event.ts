
import { Schema, model, Document } from 'mongoose';
const EventSchema = new Schema({
    event_name: String,
    max_quantity: Number,
    issued: Number,
    editingCount: Number,
    user_editing: Number,
    isEditable: Boolean
});
export default model('Event', EventSchema)