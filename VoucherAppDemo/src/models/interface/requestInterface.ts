import * as Hapi from "@hapi/hapi"

export interface ICredentials extends Hapi.AuthCredentials {
    id: string;
}

export interface IRequestAuth extends Hapi.RequestAuth {
    credentials: ICredentials;
}
export interface IRequest extends Hapi.Request {
    auth: IRequestAuth;
}
export interface EditableInterface extends IRequest {
    payload: {
        userId: Number;
        eventId: string;
    };
}
export interface LoginInterface extends IRequest {
    payload: {
        userName: String;
        password: String;
    }
}