
export interface EditableInterface {
    payload: {
        user_id: number;
        event_id: string;
    };
}