import {Schema,model, Document} from 'mongoose';
const VoucherSchema =  new Schema({
    event_id:String,
    code:String,
    value:Number
});

export default model('Voucher',VoucherSchema)