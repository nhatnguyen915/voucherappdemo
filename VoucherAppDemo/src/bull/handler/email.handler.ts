import Queue, { Job } from 'bull';
import { sendEmail } from "../../utils/mailer"

const emaiQueue = new Queue('EmailSendingQueue', {
  limiter: {
    max: 1000,
    duration: 60000,
  },
});
emaiQueue.process(async (job: Job) => {
  return await send(job);
});

emaiQueue.on('progress', function (job, progress) {
  console.log(`Job ${job.id} is ${progress * 100}% ready!`);
});

emaiQueue.on('completed', function (job, result) {
  console.log(`Job ${job.id} completed! Result: ${result}`);
});
emaiQueue.on('failed', function (job, err) {
  console.log(`Job ${job.id} failed! Error: ${err}`);
});

export const addEmailToQueue = async () => {
  const job = await emaiQueue.add({
    email: 'nhatnguyen915'
  });
  return job;
};

export const send = async (job: Job) => {
  return new Promise((resolve, reject) => {
    try {
      console.log("Send Email")
      sendEmail();
      resolve(job.data);
    } catch (error) {
      reject(error);
    }
  });
}

